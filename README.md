# RaspiTouch
This repository is used to provide the working files for my touch screeen freindly interface that is compatible with the offical Raspberry Pi 7" touch screen.  
## Setup Instructions
**Open raspi-config and prep for install**
```
sudo raspi-config
1. Change User Password
2. Network Options -> WiFi (If you are not using wired connection)
5. Interfacing options -> Turn on camera and SSH (Optional)
```
**Install git**  
```
sudo apt-get update; sudo apt-get install git
```
**Clone the Repository**  
```
git clone https://bitbucket.org/jacobs182/raspitouch.git -branch installer raspitouch-installer
```
**Make the scripts executable**
```
sudo chmod +x raspitouch-installer -R
```
**Run the Installer**  
```
raspitouch-installer/install
```
**Reboot when installer finishes**
```
sudo reboot
```
## Dependency List  
**fbi (Frame Buffered Image):** To display splash screen at boot.  
**expect:** User input imitator for updating passwords.  
**xterm xinit:** Display session manager. Makes it posible to show a user interface on Raspian lite installs.  
**openjdx-11-jre:** To run Java which is what RaspiTouch is written in.  
**openbox:** Window manager for controling window stacking.  
**samba:** To make it posible to connect via hostname. Makes it posible to upload and remove files, to/from the userdata directory, from another computer.  
**pulseaudio pulseaudio-module-bluetooth:** For routing audio to blutooth devices.  
**apache2 libapache2-mod-php:** Web based file manager.   
**cups:** For managing and using printer.  
**mpg123:** For playing mp3 files.  
**fuse-exfat** Used to mount USB drives that are formated as exfat.    
  
**Disclamer:** Apt may install dependencies of the software listed above.